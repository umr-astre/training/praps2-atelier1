library(targets)
library(tarchetypes)
source("src/functions.R")
options(tidyverse.quiet = TRUE)

## Uncomment below to use local multicore computing
## when running tar_make_clustermq().
# options(clustermq.scheduler = "multicore")

# Uncomment below to deploy targets to parallel jobs
# on a Sun Grid Engine cluster when running tar_make_clustermq().
# options(clustermq.scheduler = "sge", clustermq.template = "sge.tmpl")


tar_option_set(
  packages = c(
    "extraDistr",
    "furrr",
    "here",
    "hrbrthemes",
    "mdthemes",
    "janitor",
    "kableExtra",
    "knitr",
    "osmdata",
    "readxl",
    "rnaturalearth",
    "sf",
    "shiny",
    "tidyverse",
    "tmap"
  )
)

## Interactive workflow
# pacman::p_load(char = tar_option_get("packages"))
# tar_load(everything())


## Define the pipeline. A pipeline is just a list of targets.
list(

  # SOURCE FILES ------------------------------------------------------------

  # tar_target(
  #   data_file,
  #   "data/dataset.xlsx",
  #   format = "file"
  # )
  # ,


  # SOURCE DATA ---------------------------------------------------

  ## Data dictionary
  tar_file_read(
    data_dict,
    "data/data-dictionary.csv",
    readr::read_csv(!!.x, col_types = readr::cols())
  )
  ,

  ## Table of iso codes and names of the PRAPS countries
  ## Burkina Faso, Mali, Mauritanie, Niger, Senegal, Tchad
  tar_target(
    praps_countries,
    get_praps_countries()
  )
  ,

  ## Small-resolution world countries outlines
  tar_target(
    africa,
    rnaturalearth::ne_countries(
      scale = "small",
      type = "countries",
      continent = "africa",
      returnclass = "sf"
    )
  )
  ,


  # # Populated places. Source: Natural Earth
  # # https://www.naturalearthdata.com/downloads/10m-cultural-vectors/10m-populated-places/
  # # Only 180 places for 6 countries. These are only major cities.
  # # NOT USED.
  # tar_target(
  #   pop_places,
  #   rnaturalearth::ne_download(
  #     scale = "large",
  #     type = "populated_places_simple",
  #     category = "cultural",
  #     returnclass = "sf"
  #   ) |>
  #     inner_join(praps_countries, by = c(adm0_a3 = "iso_a3")) |>
  #     st_transform(crs = praps_crs)
  # )
  # ,


  # Populated places. Source: OpenStreetMap
  tar_target(
    pop_places,
    praps_sf |>
      select(iso_a3) |>
      retrieve_populated_places()
  )
  ,


  # Administrative divisions
  tar_target(
    adm_divisions,
    expand.grid(
        Pays = praps_countries$name_fr,
        Adm = 1:4L
      ) |>
      arrange(Pays) |>
      mutate(
        Name = c(
          ## Burkina Faso
          c("Régions", "Provinces", "Départements / Communes", "Arrondissements / Secteurs / Villages"),
          ## Mali
          c("Régions / District-capitale (Bamako)", "Cercles", "Communes / Lieux", NA),
          ## Mauritanie
          c("Régions", "Wilayas", "Départements", "Communes"),
          ## Niger
          c("Régions / Communauté urbaine de Niamey", "Départements", "Communes", NA),
          ## Sénégal
          c("Régions", "Départements", "Arrondissements", "Communes d'arrondissement / Communes / Communautés rurales"),
          ## Tchad
          c("Provinces", "Départements", "Communes / Communes d'arrondissement", NA)
        ),
        Number = c(
          13, 45, 351, NA,              # BF
          8 + 1, 49, 703, NA,           # Mali
          6, 12, 52, 216,               # Mauritanie
          7 + 1, 63, 265, NA,           # Niger
          14, 46, 133, 46 + 113 + 370,  # Sénégal
          23, 107, NA, NA               # Tchad
        )
      )
  )
  ,


  # Gridded Livestock of the World data
  tar_files_input(
    glw_files,
    files = list.files(
      "data",
      pattern = "^5_.._2010_Da.tif",
      full.names = TRUE
    )
  )
  ,
  tar_target(
    glw,
    terra::rast(glw_files) |>
      setNames(c("Cattle", "Goat", "Sheep")) |>
      terra::wrap()
  )
  ,

  # DERIVED DATA --------------------------------------------------

  # Custom projected Coordinate Reference System
  tar_target(
    praps_crs,
    laea_local(inner_join(africa, praps_countries, by = "iso_a3"))
  )
  ,

  # PRAPS country borders in sf format
  tar_target(
    praps_sf,
    inner_join(africa, praps_countries, by = "iso_a3") |>
      st_transform(crs = praps_crs)
  )
  ,

  # PRAPS admin-1 borders in sf format
  tar_target(
    praps_adm1,
    ne_states(
      country = praps_countries$name_en,
      returnclass = "sf"
    )
  )
  ,

  # GLW3 at PRAPS countries
  tar_target(
    glw_praps,
    terra::crop(
      terra::rast(glw),
      praps_sf |> st_transform(crs = 4326) |> terra::vect(),
      snap = "near",
      mask = TRUE
    )
  )
  ,

  # PARAMETERS ----------------------------------------------------


  # DESCRIPTION ---------------------------------------------------


  # MODELS --------------------------------------------------------


  # DIAGNOSIS -----------------------------------------------------


  # REPORTS -------------------------------------------------------

  tar_render(
  	report_html,
    "src/atelier_praps.Rmd",
    output_dir = "public",  # https://github.com/ropensci/drake/issues/742
    output_format =
      rmdformats::readthedown(
        dev = "CairoPNG",
        toc_depth = 3,
        lightbox = T,
        gallery = T,
        use_bookdown = T,
        number_sections = T),
    output_file = "public/atelier_praps.html",
      quiet = FALSE
  )
  ,

  tar_render(
  	report_pdf,
    "src/atelier_praps.Rmd",
    output_dir = "reports",  # https://github.com/ropensci/drake/issues/742
    output_format =
      bookdown::pdf_document2(
        dev = "pdf",
        includes = list(
          in_header = "preamble.tex",
          before_body = "before_body.tex"
        ),
        toc = T,
        toc_depth = 3,
        number_sections = T,
        latex_engine = "xelatex"
      ),
    output_file = "reports/atelier_praps.pdf",
    quiet = FALSE
  ),

  tar_render(
    annexe_technique_html,
    "src/annexe_technique.Rmd",
    output_dir = "public",  # https://github.com/ropensci/drake/issues/742
    output_format =
      rmdformats::readthedown(
        dev = "CairoPNG",
        toc_depth = 3,
        lightbox = T,
        gallery = T,
        use_bookdown = T,
        number_sections = T),
    output_file = "public/annexe_technique.html",
      quiet = FALSE
  )
  ,

  tar_render(
    annexe_technique_pdf,
    "src/annexe_technique.Rmd",
    output_dir = "reports",  # https://github.com/ropensci/drake/issues/742
    output_format =
      bookdown::pdf_document2(
        dev = "pdf",
        includes = list(
          in_header = "preamble.tex",
          before_body = "before_body.tex"
        ),
        toc = T,
        toc_depth = 3,
        number_sections = T,
        latex_engine = "xelatex"
      ),
    output_file = "reports/annexe_technique.pdf",
    quiet = FALSE
  )
  ,

  ## Slides (html files) need to be moved from src/ to public/
  ## Also copy over src/img and src/libs
  tar_render(
    slides_gestion_donnees,
    "src/gestion_donnees.Rmd",
    # output_dir = "public",  # https://github.com/ropensci/drake/issues/742
    # output_format = xaringan::moon_reader,
    # output_file = "public/gestion_donnees.html",
    quiet = FALSE
  )

  ,

  ## Slides (html files) need to be moved from src/ to public/
  ## Also copy over src/img and src/libs
  tar_render(
    slides_analyse_resultats,
    "src/analyse_resultats.Rmd",
    # output_dir = "public",  # https://github.com/ropensci/drake/issues/742
    # output_format = xaringan::moon_reader,
    # output_file = "public/gestion_donnees.html",
    quiet = FALSE
  )

)

