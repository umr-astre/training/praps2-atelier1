# Contenus formation

## 2022-05-31 Atelier Dakar


### Restitution pays

#### Sénégal PPR

- 130 / 557 comunes

- Délais campagnes sérologiques : à la fin ou après 6 mois ?

- 23 % c'est la fraction de sondage, pas un coéficient de pondération.

- Référence KoboToolBox.

- Corriger préfixe SEN_PPR_t22

#### Sénégal PPCB

- préférable de prendre 95 communes (en lieu de 120) et 15 animaux (en lieu de 12)


#### Mauritanie PPR

- Référence KoboToolBox.

- Données de stratification : couverture vaccinale (supprimer)

- 99 / 222 communes à échantillonner  (majoré depuis 68)

- ~ 792 / 5495 villages

- ~ 9504 prélèvements en total


#### Mauritanie PPCB

- 120 / 190 communes à enquêter = 63 % ?? beaucoup !!!

- 5 villages par commune et 15 animaux par village

- revenir sur les nombres de communes, villages et animaux ?

#### Niger PPR

- Tableau des communes à prélever, avec numéro de communes à virgule, et avec titre.

- Communes sélectionnées aléatoirement, et nombre de villages à prélever dans chaque une, dans le protocol pluriannuel.

- Corriger tableau des fichiers

- Référence KoboToolBox.


#### Niger PPCB

- pour rester dans le 10000 prélèvements, préférable de diminuir le nombre de communes que des prélèvements pour moins de précision mais ne pas empirer le biais.


#### Burkina Faso PPR et PPCB

- 81 / 351 communes (23 %)

- role de systèmes pastorale, abondance animale, couverture vaccinale pour la séléction de communes ?

- Bravo pour les cartes. J'espère que les fichiers cartographiques soient également preservés. Pour quoi 2 cartes ? Communes -> polygones.

- Tableaux dans le protocôle.

- 537 villages x 12 ou 15 animaux (PPR / PPCB)

- Code coleur pour exclusion -> variable


#### Niger

- Ce fichier Excel de travail il faudrait bien le mettre à propre et documenter avant le distribuer



#### Mali PPR

- "minimum" de 100 communes

- 100/701 communes (14 %)

- Références KoboToolBox


#### Mali PPCB

- Bianuelle - bisanuelle

- reviser le calcul du nombre de prélevements

- attente de kits PPCB impacte chronograme prélèvements ?


#### Tchad PPR

- Stratification par densité animale, disponibilité du paturage et de l'eau.

- 77 / 365 communes. Arrondisement à 80 communes à prélever.

- nombre de villages par commune en fonction de la zone, pas en fonction du tableau.

#### Tchad PPCB

- suppression d'une province sans bovins

- réduction à 15 animaux par village

- ajustements en fonction des connaissances d'experts ok, mais décrites

- tableau chronogramme PPCB : enquête t0 PPR ?


### Questions analyse données

- ça vaut la peine d'enqueter ppcb?

- methode de calcul probailiste pour le numéro de villages à échantillonner

- la modélisation n'est pas comprise par les politiques, mieux vaux rester au niveau des strates

- ne nous perdons pas de la réalité : ce qui importe c'est de contrôler les maladies, ne pas y aller trop loin dans la modélisation.

- mettre l'analyse dans le protocole pour travailler tous de façon homogène ?

- montrer le calcul des probabilités

- comment on calcul au niveau des strates



### Questions EPV

#### BF

- Combien de variables on doit choisir pour la stratification

- Si la commune est grande on utilise des U2. Combien ?

#### Tchad

- Traitement des troupeaux transhumants dans les U2.

#### Sénégal

- 157 Communes échantillonnées au Sénégal (toutes) et puis 5 villages max par commune. Pas envie de changer.


#### Niger

- Stratification. Tous pareil ?

- Faire une simulation avec des chiffres concrètes.



## 2022-05-27

Cyrus Nersy, Idriss Oumar Alfaroukh, Adama Diallo, Anta Diagne, M Bordier, L Manso-Silvan

PPCB :

- stratification : si possible par CV. Sinon, par régions. L'expliciter dans le protocole.

- utiliser 20 % prév. villages pour la détection PPCB

- corriger critère class positive d'une U1 (1 PI > 60; 2 entre 50 et 60)

PPR : 

- envoyer à Cyrus la planilla excel



## 2022-04-11 M Bordier. PPR.

1. Plan d'échantillonnage

  1.1. Définition de l'unité épidémiologique (UE)
  
  1.2. Nombre d'UE à préléver
  
  1.3. Répartition d'UE à préléver
  
  1.4. Nombre d'animaux à préléver à l'intérieur de chaque UE
  

2. Gestion et réalisation


3. Analyse de résultats


### 1.1. Définition de l'UE



## Faits à savoir

  - Stratégie globale PPR: Vaccination généralisée et puis on vaccine les
  nouveaux effectifs tous les ans ou tous les 2 ans.
  


## Enchainement de seuils

- Séro-positivité animal : Fonction du pourcentage d'inhibition (PI) I(PI >= tau).
  E.g. pour la PPCB, tau = 50 % selon le fabricant du kit (Idexx). 
  Variabilitée élévée du PI. Estimation de l'incertitude delta
  C.f. Projects/seromonitoring/doc/Statut sanitaire des UE.pdf.
  
- Statut UE : 

  PPR : Fonction du pourcentage d'animaux séro-positifs (PSP) dans l'échantillon,
  et de la taille de l'UE.
  
  I(PSP >= nu(n, N)), où nu(n, N) est un seuil qui dépend de la taille de 
  l'échantillon et de l'UE. Pour certains cas de n et N, nu(n, N) = NA, et le
  statut est indéterminé.
  
  PPCB: Fonction du PI, tau et delta. Dédoublé en 4 catégories (certaine posit.,
  probable posit., douteux, négatif).
  
  

## Comparaison entre pays

- les UE ne doivent pas être forcement le même niveau admin, mais doivent représenter
un risque uniforme. -> extrapolable au niveau animal.
