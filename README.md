# Premier atelier régional du projet PRAPS II

Définition et rédaction des protocoles d'enquêtes de séromonitoring de la
péripneumonie contagieuse bovine (PPCB) et de la peste des petits ruminants
(PPR).

- [Présentation : Gestion de données](https://umr-astre.pages.mia.inra.fr/training/praps2-atelier1/gestion_donnees.html)

- [Présentation : Analyse et interpŕetation des résultats](https://umr-astre.pages.mia.inra.fr/training/praps2-atelier1/analyse_resultats.html)


<!-- 
- [Notes méthodologiques](https://umr-astre.pages.mia.inra.fr/training/praps2-atelier1/atelier_praps.html)
-->

- [Annexe technique](https://umr-astre.pages.mia.inra.fr/training/praps2-atelier1/annexe_technique.html)

- Application interactive : [Évaluation du statut des unités épidémiologiques](https://famuvie.shinyapps.io/app_ppr_status/)

- Application interactive : [Sensibilité de détection de communes avec circulation de PPCB.](https://famuvie.shinyapps.io/ppcb-sensitivity/)
